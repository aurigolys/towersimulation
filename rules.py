from Match import *
from Player import *

class Rules():

    LOWER_LIMIT_TO_BE_DEMOTED = 3
    FLOOR_MASTER_FLOOR_THREASHOLD = 0

    @staticmethod
    def updatePlayerStatus(matches, floor):
        if floor.floorNumber < Rules.FLOOR_MASTER_FLOOR_THREASHOLD :
            return Rules.noFloorMasterUntilLvl(matches, floor)
        else:
            return Rules.updatePlayerStatusWithFloorMasterActive(matches, floor)

    @staticmethod
    def updatePlayerStatusWithFloorMasterActive(matches, floor):
        playersThatShouldBeSentToPreviousFloor = []
        playersThatShouldBeSentToNextFloor = []
        matchesCopy = matches.copy()
        for match in matches :
            currentMatch = matchesCopy.pop()
            #print(currentMatch.player1.name + " vs " + currentMatch.player2.name + " is there match valid ? " + str(currentMatch.isMatchValid()))
            if currentMatch.isMatchValid() :
                winner, loser = currentMatch.matchResult()
                if currentMatch.type == MatchType.FLOOR_MASTER_VS_FLOOR_MASTER :
                    playersThatShouldBeSentToNextFloor.append(winner)
                    if (Rules.shouldLoserBeSendToPreviousFloor(loser, floor.floorNumber)):
                        playersThatShouldBeSentToPreviousFloor.append(loser)         
                elif currentMatch.type == MatchType.FLOOR_MASTER_VS_CHALLENGER :
                    if winner.playertype == PlayerType.FLOOR_MASTER :
                        playersThatShouldBeSentToNextFloor.append(winner)
                        if (Rules.shouldLoserBeSendToPreviousFloor(loser, floor.floorNumber)):
                            playersThatShouldBeSentToPreviousFloor.append(loser)
                    else:
                        Rules.promotePlayer(winner)
                        if (Rules.shouldLoserBeSendToPreviousFloor(loser, floor.floorNumber)):
                            playersThatShouldBeSentToPreviousFloor.append(loser)
                        else:
                            Rules.demotePlayer(loser)
                elif currentMatch.type == MatchType.CHALLENGER_VS_CHALLENGER :
                    Rules.promotePlayer(winner)
                    if (Rules.shouldLoserBeSendToPreviousFloor(loser, floor.floorNumber)):
                        playersThatShouldBeSentToPreviousFloor.append(loser)
                elif currentMatch.type == MatchType.PLAYER_VS_PLAYER :
                    if (Rules.shouldPlayerBePromotedToChallenger(winner, floor.floorNumber)):
                        Rules.promotePlayer(winner)
                    if (Rules.shouldLoserBeSendToPreviousFloor(loser, floor.floorNumber)):
                        playersThatShouldBeSentToPreviousFloor.append(loser)
        return playersThatShouldBeSentToNextFloor, playersThatShouldBeSentToPreviousFloor

    @staticmethod
    def noFloorMasterUntilLvl(matches, floor):
        playersThatShouldBeSentToPreviousFloor = []
        playersThatShouldBeSentToNextFloor = []
        matchesCopy = matches.copy()
        for match in matches :
            currentMatch = matchesCopy.pop()
            #print(currentMatch.player1.name + " vs " + currentMatch.player2.name + " is there match valid ? " + str(currentMatch.isMatchValid()))
            if currentMatch.isMatchValid() :
                winner, loser = currentMatch.matchResult()
                if currentMatch.type == MatchType.CHALLENGER_VS_CHALLENGER :
                    playersThatShouldBeSentToNextFloor.append(winner)
                    if (Rules.shouldLoserBeSendToPreviousFloor(loser, floor.floorNumber)):
                        playersThatShouldBeSentToPreviousFloor.append(loser)
                elif currentMatch.type == MatchType.PLAYER_VS_PLAYER :
                    if (Rules.shouldPlayerBePromotedToChallenger(winner, floor.floorNumber)):
                        Rules.promotePlayer(winner)
                    if (Rules.shouldLoserBeSendToPreviousFloor(loser, floor.floorNumber)):
                        playersThatShouldBeSentToPreviousFloor.append(loser)
        return playersThatShouldBeSentToNextFloor, playersThatShouldBeSentToPreviousFloor

    @staticmethod
    def demotePlayer(player):
        if player.playertype != PlayerType.PLAYER:
            player.playertype = player.playertype / 2 #because FM is 4, C is 2 and P is 1

    @staticmethod
    def promotePlayer(player):
        if player.playertype != PlayerType.FLOOR_MASTER:
            player.playertype = player.playertype * 2 #because FM is 4, C is 2 and P is 1

    @staticmethod
    def shouldLoserBeSendToPreviousFloor(player, floorNumber):
        return (player.defeat == Rules.LOWER_LIMIT_TO_BE_DEMOTED + floorNumber)

    @staticmethod
    def shouldPlayerBePromotedToChallenger(player, floorNumber):
        return (player.hasWinThisRound and (player.victory > floorNumber))

    @staticmethod
    def isNextFloorOpenYet(currentFloor):
        return (currentFloor.activePlayerOnThisFloor() >= MINIMUM_NUMBER_OF_PLAYER_TO_OPEN_NEXT_FLOOR)