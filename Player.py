from enum import *
import math

LIMIT_FOR_DAN_PLAYER = 9

class PlayerType(IntEnum):
    PLAYER = 1
    CHALLENGER = 2
    FLOOR_MASTER = 4

class Player:
    def __init__(self, name, victory = 0, defeat = 0, playertype = PlayerType.PLAYER, lvl=5):
        self.name = name
        self.victory = victory
        self.defeat = defeat
        self.hasWinThisRound = False
        self.playertype = playertype
        self.lvl = lvl

    def wins(self):
        self.victory = self.victory + 1
        self.hasWinThisRound = True

    def loses(self):
        self.defeat = self.defeat + 1
        self.hasWinThisRound = False

    def reset(self):
        self.victory = 0
        self.defeat = 0
        self.hasWinThisRound = False
        self.playertype = PlayerType.PLAYER

    def printname(self):
        print(self.name + " win: " + str(self.victory)+ " defeat: " + str(self.defeat))

    def computeProbaOfWinning(self, opponent):
        IAmPlayerWithLowerRanking = True
        rankingDifference = self.lvl - opponent.lvl
        if rankingDifference < 0 :
            rankingDifference = -1 * rankingDifference
            IAmPlayerWithLowerRanking = False
        probaOfWinning = 1 / (math.exp(rankingDifference) + 1) #the formula has been taken from here https://en.wikipedia.org/wiki/Go_ranks_and_ratings S_{E}(A)={\frac {1}{e^{D/a}+1}}
        if IAmPlayerWithLowerRanking :
            print("player " + self.name + " is " + str(self.lvl) + " opponent is " + str(opponent.lvl) + " " + self.name + " has " + str(int(probaOfWinning*100)) + "% chances of winning")
            return probaOfWinning * 100
        else:
            return (1 - probaOfWinning) * 100


class Rank(IntEnum):
    D9 = 0
    D8 = 1
    D7 = 2
    D6 = 3
    D5 = 4
    D4 = 5
    D3 = 6
    D2 = 7
    D1 = 8
    K1 = 9
    K2 = 10
    K3 = 11
    K4 = 12
    K5 = 13
    K6 = 14
    K7 = 15
    K8 = 16
    K9 = 17
    K10 = 18
    K11 = 19
    K12 = 20
    K13 = 21
    K14 = 22
    K15 = 23
    K16 = 24
    K17 = 25
    K18 = 26
    K19 = 27
    K20 = 28
    K21 = 29
    K22 = 30
    K23 = 31
    K24 = 32
    K25 = 33
    K26 = 34
    K27 = 35
    K28 = 36
    K29 = 37
    K30 = 38