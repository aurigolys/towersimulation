class Tower:
    def __init__(self, floors):
        self.floors = floors


    def ticktack(self):
        for f in self.floors :
            if f.activePlayerOnThisFloor() == 0:
                dataPoint = (f.floorNumber, f.activePlayerOnThisFloor)
            else:
                playersThatShouldBeSentToNextFloor, playersThatShouldBeSentToPreviousFloor = f.fight()
                if len(playersThatShouldBeSentToNextFloor) > 0 :
                    if not f.isUppestFloor() :
                        self.floors[f.giveUpperFloorNumber()].addPlayers(playersThatShouldBeSentToNextFloor)
                        f.removePlayers(playersThatShouldBeSentToNextFloor)
                    else:
                        f.resetPlayers(playersThatShouldBeSentToNextFloor)
                
                if len(playersThatShouldBeSentToPreviousFloor) > 0 :
                    if not f.isLowestFloor() :
                        self.floors[f.giveLowerFloorNumber()].addPlayers(playersThatShouldBeSentToPreviousFloor)                        
                        f.removePlayers(playersThatShouldBeSentToPreviousFloor)
                    else:
                        f.resetPlayers(playersThatShouldBeSentToPreviousFloor)
                playersThatShouldBeSentToNextFloor.clear()
                playersThatShouldBeSentToPreviousFloor.clear()

                

    
        