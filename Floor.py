import random
from Player import Player
from Match import Match
from rules import *



NUMBER_OF_FLOORS = 15

class Floor:
    def __init__(self, number, players = []):
        self.floorNumber = int(number)
        self.players = players

    def activePlayerOnThisFloor(self):
        return len(self.players)

    def getNumberOfkindOfPlayers(self, typeOfPlayer):
        res = 0
        for player in self.players:
            if player.playertype == typeOfPlayer:
                res += 1
        return res

    def getNumberOfPlayers(self):
        return self.getNumberOfkindOfPlayers(PlayerType.PLAYER)    

    def getNumberOfChallenger(self):
        return self.getNumberOfkindOfPlayers(PlayerType.CHALLENGER)

    def getNumberOfFloorMaster(self):
        return self.getNumberOfkindOfPlayers(PlayerType.FLOOR_MASTER)

    def giveUpperFloorNumber(self):
        res = self.floorNumber + 1
        if res > (NUMBER_OF_FLOORS - 1):
            res = (NUMBER_OF_FLOORS - 1)
        return res

    def isLowestFloor(self):
        return self.floorNumber == 0

    def isUppestFloor(self):
        return self.floorNumber == NUMBER_OF_FLOORS

    def giveLowerFloorNumber(self):
        res = self.floorNumber - 1
        if res < 0:
            res = 0
        return res

    def addPlayers(self, players):
        for p in players:
            self.addPlayer(p)

    def addPlayer(self, player):
        player.reset()
        self.players.append(player)

    def removePlayer(self, player):
        self.players.remove(player)

    def removePlayers(self, players):
        for player in players:
            self.removePlayer(player)

    def fight(self):
        playersForChallenge = []
        matches = self.SetMatches()
        return Rules.updatePlayerStatus(matches, self)

    def SetMatches(self):
        playersForChallenge = self.players.copy()
        matches = []
        rangeMax = len(playersForChallenge)
        if len(self.players) % 2 == 1 :
            rangeMax = rangeMax - 1
        for x in range(int(rangeMax/2)):
            limitForRandomNumber = len(playersForChallenge) - 1
            limitForRandomNumber_1 = len(playersForChallenge) - 2
            playerForChallenge_1 = playersForChallenge.pop(random.randint(0, limitForRandomNumber))
            playerForChallenge_2 = playersForChallenge.pop(random.randint(0, limitForRandomNumber_1))
            matches.append(Match(playerForChallenge_1, playerForChallenge_2, self.floorNumber))
        return matches

    def resetPlayers(self, players):
        for player in players :
            player.reset()

    def print(self):
        print("Floor #" + str(self.floorNumber))
        if len(self.players) > 0:
            print("\nPlayers (" + str(len(self.players)) + ")")
            for p in self.players:
                p.printname()
        print("There is " + str(len(self.players)))
