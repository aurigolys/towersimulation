from Player import *
from enum import *
import random

class MatchType(IntEnum):
    FLOOR_MASTER_VS_FLOOR_MASTER = PlayerType.FLOOR_MASTER + PlayerType.FLOOR_MASTER            # 8
    FLOOR_MASTER_VS_CHALLENGER = PlayerType.FLOOR_MASTER + PlayerType.CHALLENGER                # 6
    CHALLENGER_VS_CHALLENGER = PlayerType.CHALLENGER + PlayerType.CHALLENGER                    # 4
    PLAYER_VS_PLAYER = PlayerType.PLAYER + PlayerType.PLAYER                                    # 2
    CHALLENGER_VS_PLAYER = PlayerType.CHALLENGER + PlayerType.PLAYER                            # 3
    FLOOR_MASTER_VS_PLAYER = PlayerType.FLOOR_MASTER + PlayerType.PLAYER                        # 5

class Match():

    matchCounter = 0

    def __init__(self, player1, player2, floorNumber):
        self.player1 = player1
        self.player2 = player2
        self.floorNumber = floorNumber
        self.type = self.getMatchType()
        if self.isMatchValid():
            Match.matchCounter = Match.matchCounter + 1

    def isMatchValid(self):
        return self.type != MatchType.FLOOR_MASTER_VS_PLAYER and self.type != MatchType.CHALLENGER_VS_PLAYER

    def matchResult(self):
        p_player1Wins = self.player1.computeProbaOfWinning(self.player2)
        if random.randint(0, 100) < p_player1Wins :
            self.player1.wins()
            self.player2.loses()
            winner = self.player1
            loser = self.player2
        else:
            self.player1.loses()
            self.player2.wins()
            winner = self.player2
            loser = self.player1
        return winner, loser

    def getMatchType(self):
        return self.player1.playertype + self.player2.playertype