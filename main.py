from Player import Player
from Floor import *
from Tower import Tower
from Match import Match
import matplotlib.pyplot as plt
import numpy as np

NUMBER_OF_PLAYERS = 106
NUMBER_OF_ROUNDS = 6

#je sais pas ou mettre ca ... 
mu = Rank.K7
sigma = 4
s = np.random.default_rng().normal(mu, sigma,NUMBER_OF_PLAYERS)
for x in range(len(s)):
    s[x] = int(s[x])
labels = []
for x in range(39):
    if x < LIMIT_FOR_DAN_PLAYER :
        labels.append(str(LIMIT_FOR_DAN_PLAYER - x) + 'd')
    else :
        labels.append(str(x - LIMIT_FOR_DAN_PLAYER + 1) + 'k')
count, bins, ignored = plt.hist(s, density=True,  ec='black')
bin_centers = 0.5 * np.diff(bins) + bins[:-1]
for count, x in zip(count, bin_centers):
    plt.gca().annotate(labels[int(x)], xy=(int(x), 0), xycoords=('data', 'axes fraction'),
        xytext=(0, -18), textcoords='offset points', va='top', ha='center')
plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi)) * np.exp( - (bins - mu)**2 / (2 * sigma**2)), linewidth=2, color='red')
plt.gca().invert_xaxis()
plt.xticks(s,"")
plt.show()
#jusque la... 

players = []
floors = []

for x in range(NUMBER_OF_PLAYERS):
    p = Player(str(x), 0, 0, lvl = int(s[x]))
    players.append(p)

for x in range(NUMBER_OF_FLOORS):
    f = Floor(x, [])
    floors.append(f)

floors[0].addPlayers(players)

dataPoints = []
for x in range(NUMBER_OF_FLOORS):
    dataPoints.append([])

totalNumberOfPlayer = 0
for f in floors:
    totalNumberOfPlayer += f.activePlayerOnThisFloor()
print("# of player at the beginning : " + str(totalNumberOfPlayer))

floor_0_normal_player = []
floor_0_challenger = []
floor_0_floor_master = []

Tower = Tower(floors)
for x in range(NUMBER_OF_ROUNDS):    
    for y in range(NUMBER_OF_FLOORS):
        dataPoints[y].append((x, Tower.floors[y].activePlayerOnThisFloor()))
        if y == 0 :
            floor_0_normal_player.append((x, Tower.floors[y].getNumberOfkindOfPlayers(PlayerType.PLAYER)))
            floor_0_challenger.append((x, Tower.floors[y].getNumberOfkindOfPlayers(PlayerType.CHALLENGER)))
            floor_0_floor_master.append((x, Tower.floors[y].getNumberOfkindOfPlayers(PlayerType.FLOOR_MASTER)))
    Tower.ticktack()

totalNumberOfPlayer = 0
for f in floors:
    totalNumberOfPlayer += f.activePlayerOnThisFloor()
print("# of player at the end : " + str(totalNumberOfPlayer))

for y in range(NUMBER_OF_FLOORS):
    plt.plot(*zip(*dataPoints[y]), label="# Player floor : " + str(y))
    plt.legend()
plt.show()

plt.plot(*zip(*floor_0_normal_player), label="# apprenti")
plt.plot(*zip(*floor_0_challenger), label="# challenger")
plt.plot(*zip(*floor_0_floor_master), label="# floor master" )
plt.legend()
plt.show()

for x in range(NUMBER_OF_ROUNDS):
    print("Round #" + str(x))
    print("#apprenti: " + str(floor_0_normal_player[x]))
    print("#challenger: " + str(floor_0_challenger[x]))
    print("#floor master: " + str(floor_0_floor_master[x]))

print("There were " + str(Match.matchCounter) + " matches being played in this simulation")